package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.util.Constants;

import java.io.File;

public class DesktopLauncher {
	private static boolean rebuildAtlas = true;
	private static boolean drawDebugOutline = false;

	public static void main (String[] arg) {

		if(rebuildAtlas) {

			// Delete atlas files if they exists
			File image_file_atlas = new File(Constants.TEXTURE_ATLAS_OBJECTS + ".atlas");
			File image_file_png = new File(Constants.TEXTURE_ATLAS_OBJECTS + ".png");
			image_file_atlas.delete();
			image_file_png.delete();

			File ui_file_atlas = new File(Constants.TEXTURE_ATLAS_UI + ".atlas");
			File ui_file_png = new File(Constants.TEXTURE_ATLAS_UI + ".png");
			ui_file_atlas.delete();
			ui_file_png.delete();

			TexturePacker.Settings settings = new TexturePacker.Settings();
			settings.maxWidth = 1024;
			settings.maxHeight = 1024;
			settings.debug = drawDebugOutline;

            TexturePacker.process(settings,
					"raw/images", // Source files
					"images",    // destination path
					"myFirstGame.pack");
			TexturePacker.process(settings,
					"raw/images-ui",
					"images",
					"myFirstGame-ui.pack");
		}

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "MyFirstGame";

		// No longer needed
//        config.useGL20 = false;

		config.width = 800;
        config.height = 480;
		new LwjglApplication(new MyGdxGame(), config);
	}
}
