package com.mygdx.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.mygdx.game.screens.DirectedGame;
import com.mygdx.game.screens.MenuScreen;
import com.mygdx.game.screens.transitions.ScreenTransition;
import com.mygdx.game.screens.transitions.ScreenTransitionSlice;
import com.mygdx.game.util.AudioManager;
import com.mygdx.game.util.GamePreferences;
import com.mygdx.game.util.input.SimpleDirectionGestureDetector;

public class MyGdxGame extends DirectedGame {

	private static final String TAG = MyGdxGame.class.getName();

	private WorldController worldController;
	private WorldRenderer worldRenderer;

	private boolean paused;


	@Override
	public void create() {

		// Set Libgdx log level
		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		// Load assets
		Assets.instance.init(new AssetManager());

		// Load preferences for audio settings and start playing music
		GamePreferences.instance.load();
		AudioManager.instance.play(Assets.instance.music.song01);

		// Start game at menu screen
		ScreenTransition transition = ScreenTransitionSlice.init(2, ScreenTransitionSlice.UP_DOWN, 10, Interpolation.pow5Out);
		setScreen(new MenuScreen(this), transition);

	}

}