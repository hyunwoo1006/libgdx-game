package com.mygdx.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.mygdx.game.objects.BunnyHead;
import com.mygdx.game.objects.Carrot;
import com.mygdx.game.objects.Feather;
import com.mygdx.game.objects.GoldCoin;
import com.mygdx.game.objects.Rock;
import com.mygdx.game.screens.DirectedGame;
import com.mygdx.game.screens.MenuScreen;
import com.mygdx.game.screens.transitions.ScreenTransition;
import com.mygdx.game.screens.transitions.ScreenTransitionSlide;
import com.mygdx.game.util.AudioManager;
import com.mygdx.game.util.CameraHelper;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.input.SimpleDirectionGestureDetector;

import javax.script.ScriptEngine;

public class WorldController extends InputAdapter implements Disposable{

    private static final String TAG = WorldController.class.getName();

    public CameraHelper  cameraHelper;

    public Level level;
    public int lives;
    public int score;

    public float livesVisual;
    public float scoreVisual;

    // Rectangles for collision detection
    private Rectangle r1 = new Rectangle();
    private Rectangle r2 = new Rectangle();

    private float timeLeftGameOverDelay;

    private DirectedGame game;

    private boolean goalReached;
    public World b2world;

    public WorldController(DirectedGame game) {
        this.game = game;
        init();
    }

    public void init() {
        cameraHelper = new CameraHelper();
        lives = Constants.LIVES_START;
        livesVisual = lives;
        timeLeftGameOverDelay = 0;
        initLevel();
    }

    public void update (float deltaTime) {
        handleDebugInput(deltaTime);

        // If game is over, then restart the game
        if (isGameOver() || goalReached) {
            timeLeftGameOverDelay -= deltaTime;
            if (timeLeftGameOverDelay < 0) {
                backToMenu();
            }
        } else {
            handleInputGame(deltaTime);
        }


        level.update(deltaTime);
        testCollision();
        b2world.step(deltaTime, 8, 3);
        cameraHelper.update(deltaTime);


        // If game is not over and player is water then start only the level
        if(!isGameOver() && isPlayerInWater())  {
            AudioManager.instance.play(Assets.instance.sounds.liveLost);
            lives--;
            if(isGameOver()) {
                timeLeftGameOverDelay = Constants.TIME_DELAY_GAME_OVER;
            } else {
                initLevel();
            }
        }


        // Mountains scrolls at different speed
        level.mountains.updateScrollPosition(cameraHelper.getPosition());

        // Lives event
        if(livesVisual > lives) {
            livesVisual = Math.max(lives, livesVisual - 1 * deltaTime);
        }

        if(scoreVisual < score) {
            scoreVisual = Math.min(score, scoreVisual + 250 * deltaTime);
        }

    }

    @Override
    public boolean keyUp(int keycode) {

        if (keycode == Input.Keys.R) {

            // Reset game world
            init();
            Gdx.app.debug(TAG, "Game world resettled");

        } else if(keycode == Input.Keys.ENTER) {
            // toggle camera follow
            cameraHelper.setTarget(cameraHelper.hasTarget() ? null : level.bunnyHead);
            Gdx.app.debug(TAG, "Camera follow enabled: " + cameraHelper.hasTarget());
        } else if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
            backToMenu();
        }
        return false;
    }



    private Pixmap createProceduralPixmap(int width, int height) {
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);

        // Fill square with red color at 50% opacity
        pixmap.setColor(1, 0, 0, 0.5f);
        pixmap.fill();

        // Draw a yellow-colored X shap on square
        pixmap.setColor(1, 1, 0, 1);
        pixmap.drawLine(0, 0, width, height);
        pixmap.drawLine(width, 0, 0, height);

        // Draw a cyan-colored border around sqare
        pixmap.setColor(0, 1, 1, 1);
        pixmap.drawRectangle(0, 0, width, height);
        return pixmap;
    }



    private void handleDebugInput(float deltaTime) {

        if (Gdx.app.getType() != Application.ApplicationType.Desktop) return;

        if (!cameraHelper.hasTarget(level.bunnyHead)) {

            // Camera Controls (move)
            float camMoveSpeed =  5 * deltaTime;
            float camMoveSpeedAccelerationFactor = 5;
            if(Gdx.input.isKeyJustPressed(Input.Keys.SHIFT_LEFT)) {
                camMoveSpeed *= camMoveSpeedAccelerationFactor;
            }
            if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                moveCamera(-camMoveSpeed, 0);
            }
            if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                moveCamera(camMoveSpeed, 0);
            }
            if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
                moveCamera(0, camMoveSpeed);
            }
            if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                moveCamera(0,  -camMoveSpeed);
            }
            if(Gdx.input.isKeyPressed(Input.Keys.BACKSPACE)) {
                cameraHelper.setPosition(0, 0);
            }

        }

        // Camera Controls (zoom)
        float camZoomSpeed = 1 * deltaTime;
        float camZoomSpeedAccelerationFactor = 5;
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) camZoomSpeed *= camZoomSpeedAccelerationFactor;
        if (Gdx.input.isKeyPressed(Input.Keys.COMMA)) cameraHelper.addZoom(camZoomSpeed);
        if (Gdx.input.isKeyPressed(Input.Keys.PERIOD)) cameraHelper.addZoom(-camZoomSpeed);
        if (Gdx.input.isKeyPressed(Input.Keys.SLASH)) cameraHelper.setZoom(1);

    }

    private void moveCamera (float x, float y) {
        x += cameraHelper.getPosition().x;
        y += cameraHelper.getPosition().y;
        cameraHelper.setPosition(x, y);
    }

    private void initLevel() {
        scoreVisual = score;
        score = 0;
        goalReached = false;
        level = new Level(Constants.LEVEL_01);
        cameraHelper.setTarget(level.bunnyHead);
        initPhysics();
    }

    private void onCollisionBunnyHeadWithRock(Rock rock) {
        BunnyHead bunnyHead = level.bunnyHead;
        float heightDifference = Math.abs(bunnyHead.position.y - (rock.position.y + rock.bounds.height));
        if(heightDifference > 0.25f) {
            boolean hitLeftEdge = bunnyHead.position.x > (rock.position.x + rock.bounds.width / 2.0f);
            if(hitLeftEdge) {
                bunnyHead.position.x = rock.position.x + rock.bounds.width;
            } else {
                bunnyHead.position.x = rock.position.x - bunnyHead.bounds.width;
            }
            return;
        }

        switch (bunnyHead.jumpState) {
            case GROUNDED:
                break;
            case FALLING:
            case JUMP_FALLING:
                bunnyHead.position.y = rock.position.y + bunnyHead.bounds.height + bunnyHead.origin.y;
                bunnyHead.jumpState = BunnyHead.JUMP_STATE.GROUNDED;
                break;
            case  JUMP_RISING:
                bunnyHead.position.y = rock.position.y + bunnyHead.bounds.height + bunnyHead.origin.y;
                break;
        }
    }

    private void onCollisionBunnyWithGoldCoin(GoldCoin goldCoin) {
        Gdx.app.debug(TAG, "Collision | Bunny with Gold Coin | Detected");
        goldCoin.collected = true;
        AudioManager.instance.play(Assets.instance.sounds.pickupCoin);
        score += goldCoin.getScore();
        Gdx.app.log(TAG, "Gold coin collected");
    }

    private void onCollisionBunnyWithFeather(Feather feather) {
        Gdx.app.debug(TAG, "Collision | Bunny with Feather | Detected");
        feather.collected = true;
        AudioManager.instance.play(Assets.instance.sounds.pickupFeather);
        score += feather.getScore();
        level.bunnyHead.setFeatherPowerup(true);
        Gdx.app.log(TAG, "Feather collected");
    }

    private void onCollisionBunnyWithGoal() {
        Gdx.app.debug(TAG, "Collision | Bunny with Goal | Detected");
        goalReached = true;
        timeLeftGameOverDelay = Constants.TIME_DELAY_GAME_FINISHED;
        Vector2 centerPosBunnyHead = new Vector2(level.bunnyHead.position);
        centerPosBunnyHead.x += level.bunnyHead.bounds.width;
        spawnCarrots(centerPosBunnyHead, Constants.CARROTS_SPAWN_MAX, Constants.CARROTS_SPAWN_RADIUS);
    }

    private void testCollision() {
        r1.set(level.bunnyHead.position.x,
                level.bunnyHead.position.y,
                level.bunnyHead.bounds.width,
                level.bunnyHead.bounds.height);

        // Test collision : Bunny Head <-> Rocks
        for(Rock rock : level.rocks) {
            r2.set(rock.position.x, rock.position.y, rock.bounds.width, rock.bounds.height);
            if(!r1.overlaps(r2)) {
                continue;
            }
            onCollisionBunnyHeadWithRock(rock);
            // IMPORTANT : must do all collision for valid edge testing on rocks.
        }

        // Test collision : Bunny Head <-> Gold Coins
        for (GoldCoin goldCoin : level.goldCoins) {
            if(goldCoin.collected) {
                continue;
            }
            r2.set(goldCoin.position.x, goldCoin.position.y,
                    goldCoin.bounds.width, goldCoin.bounds.height);
            if (!r1.overlaps(r2))
                continue;
            onCollisionBunnyWithGoldCoin(goldCoin);
            break;
        }

        // Test Collision : Bunny Head <-> Feathers
        for( Feather feather : level.feathers) {
            if (feather.collected) {
                continue;
            }
            r2.set(feather.position.x, feather.position.y, feather.bounds.width, feather.bounds.height);
            if(!r1.overlaps(r2)) {
                continue;
            }
            onCollisionBunnyWithFeather(feather);
            break;
        }

        // Text Collision : Bunny Head <-> Goal

        // Bug fix : original code is "if(goalReached)". This seem to be incorrect because
        // condition is never entered since goalReached is set to false initially
        if(!goalReached) {
            r2.set(level.goal.bounds);
            r2.x += level.goal.position.x;
            r2.y += level.goal.position.y;
            if(r1.overlaps(r2)) {
                onCollisionBunnyWithGoal();
            }
        }
    }

    private void handleInputGame(float deltaTime) {
        if (cameraHelper.hasTarget(level.bunnyHead)) {
            // Player Movement
            if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
//                Gdx.app.debug(TAG, "Going left...");
                level.bunnyHead.velocity.x = -level.bunnyHead.terminalVelocity.x;
            } else if( Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
//                Gdx.app.debug(TAG, "Going right...");
                level.bunnyHead.velocity.x = level.bunnyHead.terminalVelocity.x;
            } else {
                // Execute auto-forward movement on non-desktop platform
                if (Gdx.app.getType() != Application.ApplicationType.Desktop) {
                    level.bunnyHead.velocity.x = level.bunnyHead.terminalVelocity.x;
                }
            }

            // Bunny Jump
            if(Gdx.input.isTouched() || Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
//                Gdx.app.debug(TAG, "Jumping....");
                level.bunnyHead.setJumping(true);
            } else {
                level.bunnyHead.setJumping(false);
            }
        }
    }

    public boolean isGameOver() {
        return lives < 0;
    }

    public boolean isPlayerInWater() {
        return level.bunnyHead.position.y < -5;
    }

    private void backToMenu() {
        // switch to menus screen
        ScreenTransition transition = ScreenTransitionSlide.init(0.75f,
                ScreenTransitionSlide.DOWN, false, Interpolation.bounceOut);
        game.setScreen(new MenuScreen(game), transition);
    }

    private void initPhysics() {
        if(b2world != null) {
            b2world.dispose();
        }
        b2world = new World(new Vector2(0, -9.81f), true);

        // Rocks
        Vector2 origin = new Vector2();
        for(Rock rock : level.rocks) {
            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.KinematicBody;
            bodyDef.position.set(rock.position);
            Body body = b2world.createBody(bodyDef);
            rock.body = body;
            PolygonShape polygonShape = new PolygonShape();
            origin.x = rock.bounds.width / 2.0f;
            origin.y = rock.bounds.height / 2.0f;
            polygonShape.setAsBox(rock.bounds.width / 2.0f, rock.bounds.height / 2.0f, origin, 0);
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = polygonShape;
            body.createFixture(fixtureDef);
            polygonShape.dispose();
        }
    }

    private void spawnCarrots(Vector2 pos, int numCarrots, float radius) {
        float carrotShapeScale = 0.5f;

        // create carrots with box2d body and fixture
        for (int i = 0; i < numCarrots; i++) {
            Carrot carrot = new Carrot();

            // calculate random spawn position, rotation, and sacle
            float x = MathUtils.random(-radius, radius);
            float y = MathUtils.random(5.0f, 15.0f);
            float rotation = MathUtils.random(0.0f, 360.0f) * MathUtils.degreesToRadians;

            float carrotScale = MathUtils.random(0.5f, 1.5f);
            carrot.scale.set(carrotScale, carrotScale);

            // create box2d body for carrot with start position and angle of rotation
            BodyDef bodyDef = new BodyDef();
            bodyDef.position.set(pos);
            bodyDef.position.add(x, y);
            bodyDef.angle = rotation;
            Body body = b2world.createBody(bodyDef);
            body.setType(BodyDef.BodyType.DynamicBody);
            carrot.body = body;

            // create rectangular shape for carrot to allow interactions (collisions) with other objects
            PolygonShape polygonShape = new PolygonShape();
            float halfWidth = carrot.bounds.width / 2.0f * carrotScale;
            float halfHeight = carrot.bounds.height / 2.0f * carrotScale;
            polygonShape.setAsBox(halfWidth * carrotShapeScale, halfHeight * carrotShapeScale);

            // set physics attribute
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = polygonShape;
            fixtureDef.density = 50;
            fixtureDef.restitution = 0.5f;
            fixtureDef.friction = 0.5f;
            body.createFixture(fixtureDef);
            polygonShape.dispose();

            // finally, add new carrot to list for updating/rendering
            level.carrots.add(carrot);

        }
    }

    @Override
    public void dispose() {
        if(b2world != null) {
            b2world.dispose();
        }
    }
}
